package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int arithmeticMeanAny = 0;

        Scanner input = new Scanner(System.in); // оголошується Scanner
        System.out.println("Enter array length: ");
        int size = input.nextInt(); // зчитується розмір масиву та заноситься в size
        int array[] = new int[size]; // створюється масив int розміром size
        System.out.println("Insert array elements:");

        // заповнюється масив
        for (int i = 0; i < size; i++) {
            array[i] = input.nextInt();
        }
        System.out.print("Inserted array elements:");

        for (int i = 0; i < size; i++) {
            System.out.print(" " + array[i]);
        }
        System.out.println();

        for (int i : array) {
            arithmeticMeanAny += i;
        }
        System.out.println("Середнє арифметичне: " + arithmeticMeanAny / array.length);
    }
}
